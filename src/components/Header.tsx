"use client";
import { useSession } from "next-auth/react";
import Link from "next/link";
import { signOut } from "next-auth/react";

export default function Header() {
  const c = useSession();
  console.log("session: ", c);
  const isLogined = c.status === "authenticated";
  return (
    <div>
      {isLogined ? (
        <button onClick={() => signOut()}>Sign out</button>
      ) : (
        <Link href="/login">Login</Link>
      )}
    </div>
  );
}
