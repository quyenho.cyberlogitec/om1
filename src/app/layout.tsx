import { Inter } from "next/font/google";
import "./globals.css";
import { NextAuthProvider } from "./NextAuthProvider";
import Header from "@/components/Header";

const inter = Inter({ subsets: ["latin"] });

export default async function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <NextAuthProvider>
        <body className={inter.className}>
          <Header />
          {children}
        </body>
      </NextAuthProvider>
    </html>
  );
}
