// app>api>auth>register>route.ts

import axios from "axios";
import { NextResponse } from "next/server";

export async function POST(request: Request) {
  try {
    const { email, password } = await request.json();

    const response = await axios.post(
      "http://localhost:3001/v1/auth/register",
      { email, password }
    );

    console.log("response: "), response;
  } catch (e) {
    console.log({ e });
  }

  return NextResponse.json({ message: "success" });
}
