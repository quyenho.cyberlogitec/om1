import NextAuth from "next-auth/next";
import CredentialsProvider from "next-auth/providers/credentials";
// import { sql } from "@vercel/postgres";
// import { compare } from "bcrypt";
import axios from "axios";

const handler = NextAuth({
  pages: {
    signIn: "/login",
  },
  session: {
    strategy: "jwt",
  },
  providers: [
    CredentialsProvider({
      name: "credentials",
      credentials: {
        username: { label: "Username", type: "text" },
        password: { label: "Password", type: "password" },
      },
      async authorize(credentials, req) {
        try {
          const response = await axios.post(
            "http://localhost:3001/v1/auth/login",
            credentials
          );

          return response.data;
        } catch (error) {
          return null;
        }
      },
    }),
  ],
  callbacks: {
    async jwt({ user, token }) {
      if (user) {
        token.access_token = user.token.accessToken;
        token.refresh_token = user.token.refreshToken;
        token.expiresIn = user.token.expiresIn;
        token.user = user.user;
      }
      return token;
    },

    async session({ session, token, user }) {
      session.access_token = token.access_token;
      session.user = token.user;

      return session;
    },
  },
});

export { handler as GET, handler as POST };
